export class ConnectionEvent {
  constructor(isConnected, message) {
    this.eventType = "ConnectionEvent";
    this.timestamp = Date.now();
    this.isConnected = isConnected;
    this.message = message;
  }
}

export class LegoMessageEvent {
  constructor(legoMessage) {
    this.eventType = "LegoMessage";
    this.timestamp = Date.now();
    this.legoMessage = legoMessage;
  }
}

export class RobotSelectedEvent {
  constructor(robotId) {
    this.eventType = "RobotSelected";
    this.robotId = robotId;
  }
}
