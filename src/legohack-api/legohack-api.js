import {EventAggregator} from 'aurelia-event-aggregator';
import {inject} from "aurelia-framework";
import {SolaceClient} from "../lib/solace-client";
import {ConnectionEvent, LegoMessageEvent, RobotSelectedEvent} from "./Events";
import {BeepMessage, HelloMessage, MoveMessage} from "./Messages";

@inject(SolaceClient, EventAggregator)
export class LegohackApi {
  selectedRobotId = null;
  connection_state = "Disconnected";
  connection_last_error = "";

  constructor(solaceClient, ea) {
    this.solaceClient = solaceClient
    this.ea = ea
  }

  isConnected() {
    return this.connection_state === "Connected";
  }

  connect(connectionConfig) {
    this.solaceClient
      .connect(connectionConfig)
      .then(() => {
        this.connection_state = "Connected"

        console.log('Subscribing to lego/robot/>');
        this.ea.publish( new ConnectionEvent(true, "Connected"));

        this.solaceClient.subscribe(
          'lego/robot/*/events/hello',
          msg => {
            let legoMessage = JSON.parse(msg.getBinaryAttachment());
            legoMessage.topic = msg.getDestination();
            this.onLegoMessage(legoMessage);
          }
        );

        this.solaceClient.subscribe(
          'lego/robot/*/events/heartbeat',
          msg => {
            let legoMessage = JSON.parse(msg.getBinaryAttachment());
            legoMessage.topic = msg.getDestination();
            this.onLegoMessage(legoMessage);
          }
        );
      })
      .catch(error => {
        this.connection_state = "Failed";
        this.connection_last_error = error;

        console.log("Connection error: "+ `${error}!`);
        this.ea.publish( new ConnectionEvent(false, this.connection_last_error));
      });

    this.connection_state = "Connecting";
  }

  onLegoMessage(legoMessage) {
    this.subscribedMessage = legoMessage;
    this.ea.publish(new LegoMessageEvent(legoMessage))
  }

  setSelectedRobotId(selectedRobotId) {

    if (this.selectedRobotId) {
      this.solaceClient.unsubscribe('lego/robot/'+this.selectedRobotId+'/>');
    }

    this.selectedRobotId = selectedRobotId;
    this.ea.publish( new RobotSelectedEvent(selectedRobotId));
    this.solaceClient.subscribe(
      'lego/robot/'+selectedRobotId+'/>',
      msg => {
        let legoMessage = JSON.parse(msg.getBinaryAttachment());
        legoMessage.topic = msg.getDestination();
        this.onLegoMessage(legoMessage);
      }
    );
  }

  sendHelloMessage() {
    console.log("Hello to robot!")
    let helloMessage = new HelloMessage();
    this.sendMessage("lego/controller/events/hello", helloMessage);
  }

  sendForwardMessage() {
    let moveForwardMessage = new MoveMessage("up", 500);
    this.sendMessage("lego/robot/"+this.selectedRobotId+"/commands/move", moveForwardMessage);
  }

  sendBackwardMessage() {
    let moveBackwardMessage = new MoveMessage("down", 500);
    this.sendMessage("lego/robot/"+this.selectedRobotId+"/commands/move", moveBackwardMessage);
  }

  sendLeftMessage() {
    let moveLeftMessage = new MoveMessage("left", 500);
    this.sendMessage("lego/robot/"+this.selectedRobotId+"/commands/move", moveLeftMessage);
  }

  sendRightMessage() {
    let moveRightMessage = new MoveMessage("right", 500);
    this.sendMessage("lego/robot/"+this.selectedRobotId+"/commands/move", moveRightMessage);
  }

  sendBeepMessage() {
    let beepMessage = new BeepMessage();
    this.sendMessage("lego/robot/"+this.selectedRobotId+"/commands/beep", beepMessage);
  }

  sendMessage(topic, message) {
    this.solaceClient.publish(topic, JSON.stringify(message));
  }
}
