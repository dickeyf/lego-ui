export class HelloMessage {
  constructor() {
    this.messageType = "Hello";
    this.timestamp = Date.now();
  }
}

export class BeepMessage {
  constructor() {
    this.messageType = "Beep";
    this.timestamp = Date.now();
  }
}

export class MoveMessage {
  constructor(direction, durationMs) {
    this.messageType = "Move";
    this.timestamp = Date.now();
    this.direction = direction;
    this.durationMs = durationMs;
  }
}
