import {EventAggregator} from 'aurelia-event-aggregator';
import {inject} from 'aurelia-framework';
import {Column} from "./components/paginated-table";
import {LegohackApi} from "./legohack-api/legohack-api";
import {ConnectionEvent, LegoMessageEvent} from "./legohack-api/Events"
import {Robot} from "./legohack-api/model";

@inject(LegohackApi, EventAggregator)
export class RobotTracker {
  constructor(api, ea) {
    this.api = api;
    this.ea = ea;
    this.robots = [];
    this.selected = false;
    this.tableColumns = [
      new Column("Robot ID", ["robotId"])
    ];
  }

  bind() {
    if (this.api.isConnected()) {
      // If already connected after this element is bound, publish a gratuitous Hello so robots can broadcast their Hellos.
      this.api.sendHelloMessage();
    }

    this.ea.subscribe(ConnectionEvent, event => {
        console.log("Connection Event");
        this.connected = event.isConnected;
        if (this.connected) {
          // After connecting we need to publish a gratuitous Hello so robots can broadcast their Hellos.
          // This allows us to discover robots already connected.
          this.api.sendHelloMessage();
        }
      }
    );

    this.ea.subscribe(LegoMessageEvent, event => {
      const msg = event.legoMessage;

      const regex = "lego\\/robot\\/([A-Za-z0-9]*)\\/events/heartbeat";
      const match = msg.topic.getBytes().match(regex);
      if (match && match.length === 2) {
        const thingId = match[1];
        this.processHeartbeat(thingId, msg.timestamp);
      } else if (msg.messageType === "Hello") {
        this.processHello(msg);
      }
    })
  }

  processHeartbeat(thingId, timestamp) {
    console.log("Processing Hearbeat from thing: " + thingId);
    let newRobots = this.robots.filter(robot => robot.robotId !== thingId);
    let robot = new Robot(thingId, timestamp);
    newRobots.push(robot);
    this.robots = newRobots;
  }

  processHello(msg) {
    console.log("Processing Hello from robotID: " + msg.robotId);
    let newRobots = this.robots.filter(robot => robot.robotId !== msg.robotId);
    let robot = new Robot(msg.robotId, msg.timestamp);
    newRobots.push(robot);
    this.robots = newRobots;
  }

  created() {
  }

  updateSelection(index, item) {
    console.log(index);
    console.log(item);
    this.selected = true;
    this.selectedRobot = item;
    this.api.setSelectedRobotId(item.robotId);
  }
}
