import {inject} from "aurelia-framework";
import {LegohackApi} from "./legohack-api/legohack-api";
import {EventAggregator} from "aurelia-event-aggregator";

@inject(LegohackApi, EventAggregator)
export class Keyboard {
  constructor(LegohackApi, ea) {
    this.legohackApi = LegohackApi;
    this.ea = ea;
  }

  created() {
    console.log("Keyboard component loaded.");
  }

  bind() {
  }

  onB() {
    this.legohackApi.sendBeepMessage();
  }

  onUp() {
    console.log("onUp() triggered");
    this.legohackApi.sendForwardMessage();
  }

  onDown() {
    console.log("onDown() triggered");
    this.legohackApi.sendBackwardMessage();
  }

  onLeft() {
    console.log("onLeft() triggered");
    this.legohackApi.sendLeftMessage();
  }

  onRight() {
    console.log("onRight() triggered");
    this.legohackApi.sendRightMessage();
  }

  sendMessage(topic, message) {
    this.solaceClient.publish(topic, JSON.stringify(message));
  }
}
