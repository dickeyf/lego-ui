import {EventAggregator} from 'aurelia-event-aggregator';
import {inject} from 'aurelia-framework';
import {LegohackApi} from "./legohack-api/legohack-api";
import {ConnectionEvent, LegoMessageEvent, RobotSelectedEvent} from "./legohack-api/Events"

@inject(LegohackApi, EventAggregator)
export class VideoFeed {
  constructor(api, ea) {
    this.api = api;
    this.ea = ea;
    this.lastVideoFrame = "";
  }

  bind() {
      this.ea.subscribe(ConnectionEvent, event => {
        this.connected = event.isConnected;
      });

      this.ea.subscribe(LegoMessageEvent, event => {
        const msg = event.legoMessage;
        if (msg.topic.getBytes().startsWith("lego/robot/"+this.selectedRobotId+"/events/video")) {
          this.lastFrame = "data:image/jpeg;base64,"+msg.picture;
        }
      });

      this.ea.subscribe(RobotSelectedEvent, event => {
        this.selectedRobotId = event.robotId;
      });
  }

  set lastFrame(value) {
    this.lastVideoFrame = value;
  }

  get lastFrame() {
    return this.lastVideoFrame;
  }
}
