import {inject} from "aurelia-framework";
import {EventAggregator} from "aurelia-event-aggregator";
import {ConnectionEvent} from "./legohack-api/Events";
import {LegohackApi} from "./legohack-api/legohack-api";

@inject(LegohackApi, EventAggregator)
export class SolaceConnectionStatus {
  constructor(legohackApi, ea) {
    this.legohackApi = legohackApi;
    this.ea = ea;
    this.connected = false;
  }

  created() {
    console.log("Vmr Connection component created.")
  }

  bind() {
    this.ea.subscribe(ConnectionEvent, event => {
        this.connected = event.isConnected;
      }
    )
  }
}
