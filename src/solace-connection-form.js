import {inject} from "aurelia-framework";
import {EventAggregator} from "aurelia-event-aggregator";
import {ConnectionEvent} from "./legohack-api/Events";
import {LegohackApi} from "./legohack-api/legohack-api";
import {SolaceConnectionConfig} from "./lib/solace-client";

@inject(LegohackApi, EventAggregator)
export class SolaceConnectionForm {
  constructor(legohackApi, ea) {
    this.legohackApi = legohackApi;
    this.ea = ea;
    this.connected = false;

    this.loadConnectionInfo();
  }

  loadConnectionInfo() {
    if (typeof (Storage) !== "undefined") {
      this.solaceUri = localStorage.getItem("solaceUri");
      this.vpnName = localStorage.getItem("vpnName");
      this.username = localStorage.getItem("username");
      this.password = localStorage.getItem("password");
    }
  }

  created() {
    console.log("Robot Commands component created.");
  }

  bind() {
    this.ea.subscribe(ConnectionEvent, event => {
        this.connected = event
      }
    );

    this.ea.subscribe(MessageEvent, event => {
        this.onMessageReceived(event.legoMessage);
      }
    );
  }

  onMessageReceived(message) {
    console.log("Got a message: " + message);
  }

  onConnect() {
    localStorage.setItem("solaceUri", this.solaceUri);
    localStorage.setItem("vpnName", this.vpnName);
    localStorage.setItem("username", this.username);
    localStorage.setItem("password", this.password);


    console.log("onConnect() triggered");
    this.legohackApi.connect( new SolaceConnectionConfig(
        this.solaceUri,
        this.vpnName,
        this.username,
        this.password
    ));
  }
}
