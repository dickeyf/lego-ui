import {inject, bindable, BindingEngine} from 'aurelia-framework';

export class Column {
  constructor(name, fieldName) {
    this.name = name;
    this.fieldName = fieldName;
  }
}

@inject(Element, BindingEngine)
export class PaginatedTable {
  @bindable items = [];
  @bindable columns = [];
  @bindable itemsPerPage = 15;
  @bindable currentPageIndex = 0;
  @bindable maxDisplayedPages = 15;
  @bindable selectedItemIndex = -1;
  @bindable updateSelection;

  constructor(element, bindingEngine) {
    this.element = element;
    this.bindingEngine = bindingEngine;
    this.pages = []
    this.numPages = 0;
    this.itemsSubscription = null;
  }

  bind(bindingContext, overrideContext) {
    this.parent = bindingContext;
  }

  attached() {
    this.syncSubscription(true);
  }

  detached() {
    this.syncSubscription(false);
  }

  //This is called when items receives a new array (Not when the array itself changes).
  //The binding engine is bound to the new array so we get notified when its content is updated.
  itemsChanged(newValue, oldValue) {
    this.syncSubscription(true);
  }


  syncSubscription(subscribe) {
    if (this.itemsSubscription) {
      this.itemsSubscription.dispose();
      this.itemsSubscription = null;
    }
    if (subscribe && this.items) {
      this.itemsSubscription = this.bindingEngine.collectionObserver(this.items).subscribe(() => {
        //This callback is called when the array content has changed
        this.paginateItems();
      });
      this.paginateItems();
    }
  }

  paginateItems() {
    var firstPageItemIndex = (this.itemsPerPage*this.currentPageIndex);
    var numItems = this.items.length;
    this.pages = [];
    this.numPages = Math.ceil(numItems / this.itemsPerPage);

    //Compute the pages around the current page.
    var startPage = this.currentPageIndex - Math.floor(this.maxDisplayedPages/2);
    var endPage = this.currentPageIndex + Math.floor(this.maxDisplayedPages/2);
    //Clamp it to their maximum possible values
    if (startPage<1) {
      var adjustEndBy = 1 - startPage;
      endPage += adjustEndBy;
      startPage = 1;
    }
    if (endPage > this.numPages) {
      var adjustStartBy = endPage - this.numPages;
      startPage -= adjustStartBy;
      //Clamp startPage one last time, there is nothing to adjust this time.
      startPage = startPage < 1 ? 1 : startPage;
      endPage = this.numPages;
    }

    //If the first page is not part of it, add it.
    if (startPage !== 1) {
      this.pages.push("1");
      this.pages.push("...");
    }
    for (var i=startPage; i<=endPage; i++) {
      this.pages.push(i.toString());
    }
    //If the last page is not part of it, add it.
    if (endPage !== this.numPages) {
      this.pages.push("...");
      this.pages.push(this.numPages.toString());
    }

    //Populate the current page's items
    this.pageItems = [];
    for (var currentItemIndex=firstPageItemIndex;
         this.pageItems.length<this.itemsPerPage && currentItemIndex < numItems;
         currentItemIndex++) {
      this.pageItems.push(this.items[currentItemIndex]);
    }

    return this.pageItems;
  }

  selectPage(page) {
    if (page !== "...") {
      this.currentPageIndex = parseInt(page)-1;
      this.paginateItems();
    }
  }

  prevPage() {
    if (this.currentPageIndex > 0) {
      this.currentPageIndex--;
      this.paginateItems();
    }
  }

  nextPage() {
    if (this.currentPageIndex + 1 < this.numPages) {
      this.currentPageIndex++;
      this.paginateItems();
    }
  }

  select(rowIndex) {
    var itemIndex = rowIndex + this.currentPageIndex * this.itemsPerPage;
    var item = this.items[itemIndex];
    this.onItemSelected(itemIndex, item)
    return true;
  }

  onItemSelected(itemIndex, item) {
    this.selectedItemIndex = itemIndex;
    if (this.updateSelection) {
      this.updateSelection({itemIndex: itemIndex, item: item});
    }
  }
}


