import {inject} from 'aurelia-framework';
import {EventAggregator} from "aurelia-event-aggregator";
import {ConnectionEvent} from "./legohack-api/Events";

@inject(EventAggregator)
export class App {
  constructor(ea) {
    this.ea = ea;
    this.connected = false;
  }

  bind() {
    this.ea.subscribe(ConnectionEvent, event => {
        this.connected = event.isConnected;
      }
    )
  }
}



